import { getPromptInfo } from "./utils/getPromptInfo.js";

const getOperandsArray = (count) => {
    let operandsArray = [];

    for (let i = 0; i < count; i++) {
        const operand = getPromptInfo('Введите число: ', { type: 'number' });
        operandsArray.push(operand);
    }

    return operandsArray;
}

const calcResult = (operandsArray, operator) => {
    return operandsArray.reduce((acc, item, index) => {
        if (index === 0) {
            return item;
        }

        switch (operator) {
            case "+":
                return acc + item;
            case "/":
                return acc / item;
            case "*":
                return acc * item;
            case "-":
                return acc - item;
            default:
                return acc;
        }
    }, 0)
}

// получаю арифметическое действие
const operator = getPromptInfo(
    'Введите оператор: ',
    { type: 'string', maxLength: 1, oneOf: ['+', '-', '/', '*']}
);

//получаю кол-во операндов
const operandsCount = getPromptInfo(
    'Введите количество операндов: ',
    { type: 'number', min: 2, max: 4 }
)

//получаю операнды (массив)
const operands = getOperandsArray(operandsCount);

//считаю результат
const result = calcResult(operands, operator);

alert(`${operands.join(operator)} = ${result}`);
