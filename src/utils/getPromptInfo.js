const validationMessages = {
    LESS_THEN: 'Вы ввели число меньше, чем минимально допустимое. Попробуйте еще раз!',
    MORE_THEN: 'Вы ввели число больше, чем максимально допустимое. Попробуйте еще раз!',
    TO_SHORT: 'Вы ввели не корректнное значение, значение слишком короткое, попробуйте еще раз!',
    TO_LONG: 'Вы ввели не корректнное значение, значение слишком длинное, попробуйте еще раз!',
    WRONG_TYPE: 'Вы ввели не корректнное значение, попробуйте еще раз!',
    EMPTY: 'Вы ввели не корректнное значение, значение не может быть пустым, попробуйте еще раз!'
}

export const getPromptInfo = (message, validateRule, errorMessages = validationMessages) => {
    const retryInfo = () => getPromptInfo(message, validateRule, errorMessages);
    const promptValue = prompt(message);

    if (promptValue === null) {
        return;
    }

    switch (validateRule.type) {
        case 'number':
            const validPromptValue = Number(promptValue);

            if(isNaN(validPromptValue)) {
                alert(errorMessages.WRONG_TYPE || 'WRONG_TYPE');
                return retryInfo();
            }

            if (validateRule.min !== 'undefined' && validateRule.min > validPromptValue) {
                alert(errorMessages.LESS_THEN || 'LESS_THEN');
                return retryInfo();
            }

            if (validateRule.max !== 'undefined' && validateRule.max < validPromptValue) {
                alert(errorMessages.MORE_THEN || 'MORE_THEN');
                return retryInfo();
            }

            return validPromptValue;

        case 'string':
            if (promptValue === '') {
                alert(errorMessages.EMPTY || 'EMPTY');
                return retryInfo();
            }

            if (validateRule.maxLength !== 'undefined' && promptValue.length > validateRule.maxLength) {
                alert(errorMessages.TO_LONG || 'TO_LONG');
                return retryInfo();
            }

            if (validateRule.minLength !== 'undefined' && promptValue.length < validateRule.maxLength) {
                alert(errorMessages.TO_SHORT || 'TO_SHORT');
                return retryInfo();
            }

            if (validateRule.oneOf !== 'undefined' && !validateRule.oneOf.includes(promptValue)) {
                alert(errorMessages.WRONG_TYPE || 'WRONG_TYPE');
                return retryInfo();
            }

            return promptValue;

        default:
            return promptValue;
    }
}